import 'package:flutter/material.dart';

import 'package:navagation_bar_app/src/ui/widgets/navigation_bar.dart';
import 'package:navagation_bar_app/src/ui/widgets/tab_item.dart';

//****************************** MY CLASS MAIN *********************************
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

//***************************** CLASS MAIN STATE *******************************
class _MyHomePageState extends State<MyHomePage> {

  //VARIABLE

  //***************************** OVERRIDE WIDGET MAIN *************************
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,

      //::::::::::::::::::::::::: APP BAR ::::::::::::::::::::::::::
      appBar: AppBar(
        backgroundColor: PURPLE,                  //CALL CONSTANT COLOR
        title: Text("Tab Bar Animation"),
      ),

      //::::::::::::::::::::::::: NAVIGATION BAR :::::::::::::::::::::
      bottomNavigationBar: NavigationTabBar(),    //CALL CLASS navigation_bar

      //:::::::::::::::::::::::::::: BODY :::::::::::::::::::::::::::
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
          ],
        ),
      ),
    );
  }
}
//******************************************************************************